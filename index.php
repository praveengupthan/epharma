<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>East Pharma</title>    
    <?php include 'includes/styles.php'?>
    <?php include 'includes/data.php'?>
</head>

<body>
    <?php include 'includes/header.php'?>
        <!-- master slider -->
        <?php include 'includes/Homeslider.php'?>
        <!--/ master slider -->   
        <!-- iso section -->
        <section class="isoSection">
            <p class="pb-0 text-center mb-0 aos-item" data-aos="fade-up">Facility certified with <span>ISO 9001:2015, ISO 14001:2015, ISO 45001:2018, ISO 15378:2017, ISO 13485, ISO 20072 & USDMF</span></p>
        </section>
        <!--/ iso ssectioin -->
        <!-- section Products -->
        <section class="popularProducts py-2 py-sm-5">
            <div class="container-fluid">
                <h2 class="text-uppercase text-center sectionTitle aos-item" data-aos="fade-up"><span>Popular</span> Products</h2>
                <!-- Swiper -->
                <div class="swiper-container productsSlider">
                    <div class="swiper-wrapper">
                        <!-- swiper slider -->
                        <?php
                        for($i=0; $i<count($productSliderItem);$i++){ ?>
                        <div class="swiper-slide">
                            <a href="<?php echo $productSliderItem[$i][0]?>" class="aos-item" data-aos="fade-down">
                                <img src="img/products/<?php echo $productSliderItem[$i][1]?>.jpg" alt="" class="img-fluid">
                            </a>
                            <article class="aos-item" data-aos="fade-up">
                                <h5 class="m-0 "><?php echo $productSliderItem[$i][2]?></h5>
                                <p class="pb-2 m-0 d-none"><?php echo $productSliderItem[$i][3]?></p>
                                <a href="<?php echo $productSliderItem[$i][0]?>" class="d-none text-uppercase position-relative readMorelink">Read More</a>
                            </article>
                        </div>   
                    <?php } ?>
                        <!--/ swiper slider -->                 
                    </div>
                    <!-- Add Arrows -->
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                </div>
            </div>
        </section>
        <!--/ section products -->
        <!-- why us -->
        <section class="whyus my-3">
            <div class="container-fluid py-2 py-md-4">
                <h2 class="text-uppercase text-center sectionTitle aos-item py-3" data-aos="fade-up"><span>Why </span>Choose Us</h2>
                <!-- row -->

                <div class="row g-0">
                        <div class="col-lg-3 col-md-6 aos-item" data-aos="fade-up">
                            <img src="img/whyushome01.jpg" alt="" class="img-fluid whyusimg">
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="text-center whyusCol bluecol">
                                <span class="icon-industry icomoon aos-item" data-aos="fade-up"></span>
                                <article class="aos-item" data-aos="fade-down">
                                    <h5 class="h4">State-Of-the-art facility</h5>
                                    <p>Highly sophisticated equipment installed for precision and durability.  cGMP & ISO Standards strictly adhered.</p>
                                </article>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6">
                            <div class="whychooseSlider">
                                <!-- Swiper -->
                                <div class="swiper-container whyusswiper">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide"><img src="img/whyusbanner01.jpg"></div>
                                        <div class="swiper-slide"><img src="img/whyusbanner02.jpg"></div>
                                        <div class="swiper-slide"><img src="img/whyusbanner03.jpg"></div>
                                    </div>
                                    <!-- Add Pagination -->
                                    <div class="swiper-pagination"></div>
                                </div>
                            </div>
                        </div>
                    
                    
                        <div class="col-lg-3 col-md-6">
                            <div class="text-center whyusCol greencol">
                                <span class="icon-facemask icomoon aos-item" data-aos="fade-up"></span>
                                <article class="aos-item" data-aos="fade-down">
                                    <h5 class="h4">Safety & Pristine</h5>
                                    <p>We ensure our facility is fully complying with safety and quality norms resulting in first class products and employees safety.</p>
                                </article>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 aos-item" data-aos="fade-up">
                            <img src="img/whyushome02.jpg" alt="" class="img-fluid whyusimg">
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="text-center whyusCol pinkcol">
                                <span class="icon-value icomoon aos-item" data-aos="fade-up"></span>
                                <article class="aos-item" data-aos="fade-down">
                                    <h5 class="h4">Team</h5>
                                    <p>Dedicated team of professionals with great experience are our added advantage</p>
                                </article>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 aos-item" data-aos="fade-up">
                            <img src="img/whyushome03.jpg" alt="" class="img-fluid whyusimg">
                        </div>
                        <div class="col-lg-3 col-md-12 aos-item" data-aos="fade-up">
                            <img src="img/whyushome04.jpg" alt="" class="img-fluid whyusimg">
                        </div>
                        <div class="col-md-12 col-lg-6">
                            <div class="whychooseSlider">
                                <!-- Swiper -->
                                <div class="swiper-container whyusswiper">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide"><img src="img/whyusbanner04.jpg"></div>
                                        <div class="swiper-slide"><img src="img/whyusbanner05.jpg"></div>
                                        <div class="swiper-slide"><img src="img/whyusbanner06.jpg"></div>
                                    </div>
                                    <!-- Add Pagination -->
                                    <div class="swiper-pagination"></div>
                                </div>
                            </div>
                        </div>
                    
                    
                        <div class="col-lg-3 col-md-12">
                            <div class="text-center whyusCol greencol2">
                                <span class="icon-research icomoon aos-item" data-aos="fade-up"></span>
                                <article class="aos-item" data-aos="fade-down">
                                    <h5 class="h4">R&D</h5>
                                    <p>A continual research & development is carried out for improving Stability and secure products</p>
                                </article>
                            </div>
                        </div>
                </div>
                <!--/ row -->
            </div>
        </section>
        <!--/ why us -->
    
        <!-- strengths section -->
        <section class="ourStrength">
            <!-- container fluid -->
            <div class="container">
                <div class="row">               
                    <h2 class="h2 py-md-4 text-center aoss-item" data-aos="fade-up">Our Strength</h2>                   
                    <!-- item -->
                    <?php 
                    for ($i=0; $i<count($homeStrengthItem);$i++){ ?>
                        <div class="col-md-4 text-center">
                        <div class="strengthItem">                          
                            <div class="strengthIcon mx-auto">
                                <span class="<?php echo $homeStrengthItem[$i][0]?> icomoon"></span>
                            </div>
                            <div class="text-center strengthDesc">
                                <p class="aos-item" data-aos="fade-up"><?php echo $homeStrengthItem[$i][1]?></p>
                            </div>
                        </div>                
                        <!--/ item -->      
                    </div>
                    <?php } ?>
                </div>
            </div>
            <!--/ container fluid -->
        </section>
        <!--/ strengths section-->


    <!-- footer -->
     <?php include 'includes/footer.php'?>
    <!--/ footer -->
     <?php include 'includes/scripts.php'?>

    <script>
        var swiper = new Swiper('.whyusswiper', {
        slidesPerView: 1,
        loop: true,
	    autoplay: true,
        pagination: {
            el: '.swiper-pagination',
            dynamicBullets: true,
        },
        });
    </script>     
</body>

</html>