<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Career at East Pharma</title>    
    <?php include 'includes/styles.php'?>
    <?php include 'includes/data.php'?>
</head>

<body>
    <?php include 'includes/header.php'?> 
     <main class="subPage">
         <!-- sub page header -->
         <div class="subPageHeader">
             <div class="container">
                 <div class="row">
                     <div class="col-md-6 leftsubpageHeader align-self-center">
                         <h1>Career At East Pharma</h1>
                     </div>
                     <div class="col-md-6 align-self-center">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo $homeLink?>"><?php echo $SPageHome ?></a></li>                              
                                <li class="breadcrumb-item active" aria-current="page">Career</li>
                            </ol>
                        </nav>
                     </div>
                 </div>
             </div>
         </div>
         <!--/ sub page header -->

         <!-- sub page body -->
         <div class="subpageBody">
             <div class="container">
                <!-- main row -->
                <div class="row justify-content-lg-center">
                    <!-- right section -->
                    <div class="col-lg-4">
                       <div class="careerRightSection p-3 border aos-item" data-aos="fade-up">
                            <h6>Join with us</h6>
                            <h4 class="py-3 fsbold">Submit your Application</h4>
                            <form class="form customForm">
                                <div class="form-group mb-0 pb-0">
                                    <label>Full Name</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="name" placeholder="Write Your Name">
                                    </div>
                                </div>
                                <div class="form-group mb-0 pb-0">
                                    <label>Phone Number </label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="phone" placeholder="Enter Valid Phone" >
                                    </div>
                                </div>
                                <div class="form-group mb-0 pb-0">
                                    <label>Email </label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="phone" placeholder="Enter Email" >
                                    </div>
                                </div>
                                <div class="form-group mb-0 pb-0">
                                    <label>Current Company </label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="phone" placeholder="Current Company" >
                                    </div>
                                </div>
                                <div class="form-group mb-0">
                                    <label>Send Your CV </label>
                                    <div class="input-group">
                                        <input type="file" class="form-control" name="phone" placeholder="Send Your Updated CV" >
                                    </div>
                                </div>
                                <button class="btn btn-info w-100 text-uppercase" name="submitContact">Submit Your Profile</button>          
                            </form>
                       </div>
                    </div>
                    <!--/ right section -->
                </div>
                <!--/ main row -->
                <!-- current jobs -->
                <div class="currentOpenings d-none">
                    <h6>Positions</h6>
                    <h3 class="py-3">Current Openings</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="jobListItem">
                                <h5 class="fsbold">Job Position Title will be here</h5>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. At ratione inventore consectetur cupiditate animi blanditiis ullam saepe atque expedita excepturi!</p>
                                <div class="d-flex">
                                    <p class="text-secondary py-2 pe-4">
                                        <small>Location:<span class="fsbold"> Hyderabad</span></small>
                                    </p>
                                    <p class="text-secondary py-2 pe-4">                                       
                                        <small>Experience:<span class="fsbold"> 6 years</span></small>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="jobListItem">
                                <h5 class="fsbold">Job Position Title will be here</h5>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. At ratione inventore consectetur cupiditate animi blanditiis ullam saepe atque expedita excepturi!</p>
                                <div class="d-flex">
                                    <p class="text-secondary py-2 pe-4">
                                        <small>Location:<span class="fsbold"> Hyderabad</span></small>
                                    </p>
                                    <p class="text-secondary py-2 pe-4">                                       
                                        <small>Experience:<span class="fsbold"> 6 years</span></small>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="jobListItem">
                                <h5 class="fsbold">Job Position Title will be here</h5>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. At ratione inventore consectetur cupiditate animi blanditiis ullam saepe atque expedita excepturi!</p>
                                <div class="d-flex">
                                    <p class="text-secondary py-2 pe-4">
                                        <small>Location:<span class="fsbold"> Hyderabad</span></small>
                                    </p>
                                    <p class="text-secondary py-2 pe-4">                                       
                                        <small>Experience:<span class="fsbold"> 6 years</span></small>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="jobListItem">
                                <h5 class="fsbold">Job Position Title will be here</h5>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. At ratione inventore consectetur cupiditate animi blanditiis ullam saepe atque expedita excepturi!</p>
                                <div class="d-flex">
                                    <p class="text-secondary py-2 pe-4">
                                        <small>Location:<span class="fsbold"> Hyderabad</span></small>
                                    </p>
                                    <p class="text-secondary py-2 pe-4">                                       
                                        <small>Experience:<span class="fsbold"> 6 years</span></small>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ current jobs -->


             </div>
         </div>
         <!--/ sub page body -->
     </main>
    <!-- footer -->
     <?php include 'includes/footer.php'?>
    <!--/ footer -->
     <?php include 'includes/scripts.php'?>
</body>

</html>