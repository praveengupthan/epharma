<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Quality Policy </title>    
    <?php include 'includes/styles.php'?>
    <?php include 'includes/data.php'?>
</head>

<body>
    <?php include 'includes/header.php'?>
     <main class="subPage">
         <!-- sub page header -->
         <div class="subPageHeader">
             <div class="container">
                 <div class="row">
                     <div class="col-md-6 leftsubpageHeader align-self-center">
                         <h1>Quality Policy</h1>
                     </div>
                     <div class="col-md-6 align-self-center">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo $homeLink?>"><?php echo $SPageHome?></a></li>
                                <li class="breadcrumb-item active" aria-current="page">Quality Policy</li>
                            </ol>
                        </nav>
                     </div>
                 </div>
             </div>
         </div>
         <!--/ sub page header -->

         <!-- sub page body -->
         <div class="subpageBody">
             <div class="container">
                 <!-- row -->
                 <div class="row">
                    <div class="col-md-6 aos-item" data-aos="fade-up">                       
                        <img src="img/qulaity.jpg" alt="" class="img-fluid w-100">                        
                    </div>
                    <div class="col-md-6 align-self-center aos-item" data-aos="fade-down">
                        <h6>Our Quality Policy</h6>
                        <h2 class="pt-3">Quality is our prime policy.</h2>
                        <p>We at east pharma technologies, are committed to enhance customer satisfaction by manufacturing and supply of high quality safe seals through implementation and continual improvement of quality management system</p>
                        <ul class="list-items">
                            <li>Raw materials procurement from qualified vendors.</li>
                            <li>Stringent Quality control procedures in practice from raw materials to Finished Goods.</li>
                            <li>Our systems qualify for the CGMP and FDA requirement.</li>   
                            <li>Can assure you with right blend of technology and technical people can provide you consistent quality and timely service.</li>                     
                        </ul>
                    </div>                   
                 </div>
                 <!-- row -->

                 <!-- row -->
                 <div class="row pt-3">                 
                    <div class="col-md-12">
                        <h6 class="pt-4">Award & Rewards</h6>
                        <h2 class="pt-2">Symbol of Quality</h2>  
                    </div>                  
                 </div>
                 <!--/ row -->

                    <!-- certifications -->
                    <div class="certificateImages py-3 d-flex justify-content-between">
                        <?php include 'includes/certificates.php' ?>
                    </div>
                    <!--/ certifications -->

                 <!-- row -->
                 <div class="row py-3">
                     <div class="col-md-12 aos-item" data-aos="fade-up">
                         <h2 class="py-2">Support with Regulatory Process</h2>  
                     </div>

                     <div class="col-md-9 aos-item" data-aos="fade-down">
                         <img src="img/regulatoryprocess01.jpg" alt="" class="img-fluid w-100 pb-3 regulatoryImg">
                         <h5 class="h6 py-2">Schedule internal audits</h5>
                     </div>

                     <div class="col-md-3 aos-item" data-aos="fade-up">
                         <img src="img/regulatoryprocess02.jpg" alt="" class="img-fluid w-100 pb-3 regulatoryImg">
                         <h5 class="h6 py-2">In house tool room for continual Improvement</h5>
                     </div>

                     <div class="col-md-4 aos-item" data-aos="fade-down">
                         <img src="img/regulatoryprocess03.jpg" alt="" class="img-fluid w-100 pb-3 regulatoryImg">
                         <h5 class="h6 py-2">Annual Quality and management reviews</h5>
                     </div>

                      <div class="col-md-4 aos-item" data-aos="fade-up">
                         <img src="img/regulatoryprocess04.jpg" alt="" class="img-fluid w-100 pb-3 regulatoryImg">
                         <h5 class="h6 py-2">DMF for all products</h5>
                     </div>

                     <div class="col-md-4 aos-item" data-aos="fade-down">
                         <img src="img/regulatoryprocess05.jpg" alt="" class="img-fluid w-100 pb-3 regulatoryImg">
                         <h5 class="h6 py-2">Periodic GMP & technical training to all emplyees</h5>
                     </div>
                     
                 </div>
                 <!--/ row -->



             </div>
         </div>
         <!--/ sub page body -->
     </main>
    <!-- footer -->
     <?php include 'includes/footer.php'?>
    <!--/ footer -->
     <?php include 'includes/scripts.php'?>
</body>
</html>