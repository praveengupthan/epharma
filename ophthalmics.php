<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ophthalmic for Eye Droppers</title>    
    <?php include 'includes/styles.php'?>
    <?php include 'includes/data.php'?>
</head>

<body>
    <?php include 'includes/header.php'?>
     <main class="subPage">
         <!-- sub page header -->
         <div class="subPageHeader productDetailHeader">
             <div class="container">
                 <div class="row">
                     <div class="col-lg-6 leftsubpageHeader align-self-center text-center">
                        <figure>
                            <a href="javascript:void(0)" class="customBtn" data-bs-toggle="modal" data-bs-target=".productVideo"><span class="icon-play-button"></span> </a>
                            <img src="img/products/opthalmicsbanner.jpg" alt="" class="img-fluid">
                        </figure>                        
                     </div>
                     <div class="col-lg-6 align-self-center">
                         <article class="p-3">
                            <h1 class="h3 fsbold">Ophthalmic for Eye Droppers</h1>
                            <p>Ophthalmic 3 PCs Components manufactured in East Pharma Technologies in clean room for package of Ophthalmic products.  </p>
                            <a href="javascript:void(0)" class="customBtn" data-bs-toggle="offcanvas" data-bs-target="#requestQuotation" aria-controls="offcanvasRight">Request for Quotation</a>    
                            <a href="javascript:void(0)" class="customBtn" data-bs-toggle="offcanvas" data-bs-target="#requestSample" aria-controls="offcanvasRight">Request Samples</a>
                            <p class="pt-2"> <a class="link" href="products.php"><span class="icon-arrowleft icomoon"></span> Back to Products </a></p>
                         </article>
                     </div>
                 </div>
             </div>
         </div>
         <!--/ sub page header -->

         <!-- sub page body -->
         <div class="subpageBody pt-0">
            <div class="container">                
            <header id="header" class="header" data-scrollto-offset="0">
            <!-- product detail nav -->
            <nav class="navbar navbar-productdetail mt-md-2">
                <ul class="d-md-flex justify-content-md-between align-items-center" id="mobileItems">
                    <li><a class="nav-link scrollto" href="flipoffsealsnew.php#overview">Overview</a></li>
                    <li><a class="nav-link scrollto" href="flipoffsealsnew.php#productAttributes">Sizes</a></li>
                    <li><a class="nav-link scrollto" href="flipoffsealsnew.php#downloadableResources">Downloads</a></li>
                    <li><a class="nav-link scrollto" href="flipoffsealsnew.php#certifications">Certifications</a></li>                    
                    <li><a class="nav-link scrollto" href="flipoffsealsnew.php#relatedProducts">Related Products</a></li>
                </ul>
            </nav>
            <!--/ product detail nav -->
            </header>           
             </div>
             <!-- product detail description -->
             <section class="featured-services pt-3" id="overview">
              <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="h4 fsbold border-bottom pb-2 mb-3">Overview</h2>
                    </div>         
                    <div class="col-md-12 align-self-center">
                        <h6 class="fsbold d-inline-block">Product Details </h6>
                        <ul class="list-items">
                            <li>Manufactured from LDPE and HDPE.</li>
                            <li>Available In bottles i.e,5ML, 10ML</li>
                            <li>Available Nozzles i.e., 28µL, 30µL, 45µL and 60µL.</li>
                            <li>Available in both Gamma and ETO Sterilized. </li>
                            <li>DMF Number-DMF#033005 Type-III. </li>
                            <li>ISO Class 8 Manufacturing & Packing.</li>
                            <li>Manufactured in the Facility Certified With ISO 9001:2015, ISO 15378:2017, ISO 13485 & ISO 20072.</li>
                            <li>Suitable for dispensing of Ophthalmic Products.</li>
                            <li>EPT Bottle Cap comes with Tamper Evident ring to avoid leakages and for easy removal.</li>
                        </ul>
                    </div>
                    <div class="col-md-12">   
                        <h4 class="pb-2 h4 fbold">Ophthalmic Gallery</h4>
                        <!-- gallery -->
                        <section class="gallery-block grid-gallery mt-0 pt-0 aos-item pb-0" data-aos="fade-up">
                            <div class="row g-2">
                                <div class="col-md-6 col-lg-3 col-6 item ">
                                    <a class="lightbox" href="img/products/opthalmics/opthalmics01.jpg">
                                        <img class="img-fluid image scale-on-hover" src="img/products/opthalmics/opthalmics01.jpg">
                                    </a>
                                </div>
                                <div class="col-md-6 col-lg-3 col-6 item">
                                    <a class="lightbox" href="img/products/opthalmics/opthalmics02.jpg">
                                        <img class="img-fluid image scale-on-hover" src="img/products/opthalmics/opthalmics02.jpg">
                                    </a>
                                </div>
                                <div class="col-md-6 col-lg-3 col-6 item">
                                    <a class="lightbox" href="img/products/opthalmics/opthalmics03.jpg">
                                        <img class="img-fluid image scale-on-hover" src="img/products/opthalmics/opthalmics03.jpg">
                                    </a>
                                </div>                               
                                <div class="col-md-6 col-lg-3 col-6 item">
                                    <a class="lightbox" href="img/products/opthalmics/opthalmics04.jpg">
                                        <img class="img-fluid image scale-on-hover" src="img/products/opthalmics/opthalmics04.jpg">
                                    </a>
                                </div>                                                          
                            </div>                           
                        </section>                 
                        <!--/ gallery -->
                    </div>
                    </div>
                </div>
             </section>
              <section class="featured-services" id="productAttributes">
              <div class="container">
                  <div class="row justify-content-center">
                   <div class="col-md-12">
                        <h3 class="h4 fsbold border-bottom pb-2 mb-3">Ophthalmic's (Bottle's)</h2>
                   </div>
                    <div class="col-md-12 text-center">                      
                        <div class="table-responsive pb-4">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                    <th scope="col">S.No:</th>
                                    <th scope="col" class="text-start">Product Description</th>
                                    <th scope="col">Height  </th>
                                    <th scope="col">Body Diameter </th>
                                    <th scope="col">Bore Diameter  </th>
                                    <th scope="col"> Thread Diameter  </th>
                                    <th scope="col">Overflow Capacity </th>
                                    <th scope="col">Min. Wall Thickness  </th>
                                    <th scope="col">Weight  </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td class="text-start">5ml LDPE Bottle with 13mm neck finish </td>
                                        <td>42.60 mm</td>
                                        <td>21.20 mm</td>
                                        <td>8.10 mm </td>
                                        <td>14.00 mm</td>
                                        <td>7.30 mm</td>
                                        <td>0.35 mm</td>
                                        <td>2.95 gm</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">2</th>
                                        <td class="text-start">10 ml LDPE Bottle With 13 mm Neck Finish  </td>
                                        <td>52.50 mm</td>
                                        <td>24.25 mm</td>
                                        <td>8.10 mm</td>
                                        <td>14.00 mm</td>
                                        <td>12.70 mm</td>
                                        <td>0.35 mm</td>
                                        <td>3.60 gm</td>
                                    </tr>
                                   
                                </tbody>
                            </table>
                        </div>

                        <div class="col-md-12 text-start">
                            <h3 class="h4 fsbold border-bottom pb-2 mb-3">Ophthalmic's (Nozzle's)</h2>
                        </div>

                        <div class="table-responsive pb-4">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">S.No:</th>
                                        <th scope="col" class="text-start">Product Description</th>
                                        <th scope="col">Height   </th>
                                        <th scope="col">Plug Diameter </th>
                                        <th scope="col"> Diameter of Ring  </th>
                                        <th scope="col"> Weight   </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td class="text-start">13 mm LDPE NOZZLE (White opaque) 28μl </td>
                                        <td>19.00 mm </td>
                                        <td>8.43 mm</td>
                                        <td>12.40 mm</td>
                                        <td>0.48 gm </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">2</th>
                                        <td class="text-start">13 mm LDPE NOZZLE (White opaque) 30μl</td>
                                        <td>17.00 mm</td>
                                        <td>8.43 mm</td>
                                        <td>12.40 mm</td>
                                        <td>0.41 gm </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">3</th>
                                        <td class="text-start">13 mm LDPE NOZZLE (White opaque) 45μl</td>
                                        <td>19.00 mm </td>
                                        <td>8.43 mm</td>
                                        <td>12.40 mm</td>
                                        <td>0.47 gm  </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">4</th>
                                        <td class="text-start">13 mm LDPE NOZZLE (White opaque) 60μl</td>
                                        <td>19.00 mm </td>
                                        <td>8.43 mm</td>
                                        <td>12.40 mm</td>
                                        <td>0.47 gm  </td>
                                    </tr>
                                   
                                </tbody>
                            </table>
                        </div>

                        <div class="col-md-12 text-start">
                            <h3 class="h4 fsbold border-bottom pb-2 mb-3">Ophthalmic's (Cap's)</h2>
                        </div>

                        <div class="table-responsive pb-4">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">S.No:</th>
                                        <th scope="col" class="text-start">Product Description</th>
                                        <th scope="col">Height   </th>
                                        <th scope="col">Inner Diameter  </th>
                                        <th scope="col"> Diameter of Tamper  </th>
                                        <th scope="col"> Weight </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td class="text-start">13 mm HDPE CAP (Various Colours) </td>
                                        <td>25.50 mm </td>
                                        <td>13.15 mm</td>
                                        <td>20.50 mm</td>
                                        <td>2.30 mm </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                  </div>
                </div>
              
              </section>
             
              <section class="featured-services" id="downloadableResources">
              <div class="container">
                  <div class="row justify-content-center">
                        <div class="col-md-5">
                            <h2 class="text-center pb-4">Downloadable Resources</h2>
                            <div class="d-none">                           
                            <a href="img/dummy.pdf" download class="downloadItem"><span class="icon-download"></span> Downloadable Component Name will be here</a>
                            <a href="img/dummy.pdf" download  class="downloadItem"><span class="icon-download"></span> Downloadable Component Name will be here</a>
                            </div>
                            <p class="text-center h5 fwhite">Coming soon</p>
                      </div>
                  </div>
              </div>r
              </section>
              <section class="featured-services" id="certifications">
                <div class="container">
                  <div class="row justify-content-center">
                    <div class="col-md-12">
                        <h3 class="h4 fsbold border-bottom pb-2 mb-3">Certifications</h2>
                    </div>
                  </div>

                   <!-- certifications -->
                   <div class="certificateImages py-3 d-flex justify-content-between">
                        <?php include 'includes/certificates.php' ?>
                    </div>
                    <!--/ certifications -->
                </div>
               
              </section>
              <section class="featured-services realtedProducts pt-0" id="relatedProducts">
              <div class="container">                 
                  <h3 class="h4 fsbold pb-2 mb-3">Related Products</h2>

                <div class="swiper-container w-100 productsSliderProductDetail">
                <div class="swiper-wrapper">
                    <!-- swiper slider -->
                    <?php
                    for($i=0; $i<count($productSliderItem);$i++){ ?>
                    <div class="swiper-slide">
                         <div class="card productCard mb-3">
                            <a href="<?php echo $productItem[$i][0]?>">
                                <img src="img/products/<?php echo $productItem[$i][1]?>" alt="" class="img-fluid">
                            </a>
                            <div class="card-body">
                                <h5 class="card-title"><?php echo $productItem[$i][2]?></h5>
                                <p class="card-text"><?php echo $productItem[$i][3]?></p>
                                <a href="<?php echo $productItem[$i][0]?>" class="card-link">Read More &rarr; </a>
                            </div>
                        </div>
                    </div>   
                   <?php } ?>
                    <!--/ swiper slider -->                 
                </div>
                <!-- Add Arrows -->
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
            </div>
              </div>
              </section>
             <!-- product detail description -->
         </div>
         <!--/ sub page body -->
     </main>
    <!-- footer -->
     <?php include 'includes/footer.php'?>
    <!--/ footer -->
     <?php include 'includes/scripts.php'?>

     <script>
        $(document).ready(function(){
            $("#DetailDropdown").click(function(){
                $("#mobileItems").toggle();
            });
        });
    </script>

     <script>
        window.onscroll = function() {myFunction()};
        var navbar = document.getElementById("header");
        var sticky = navbar.offsetTop;
        function myFunction() {
        if (window.pageYOffset >= sticky) {
            navbar.classList.add("stickynav")
        } else {
            navbar.classList.remove("stickynav");
        }
        }
        </script>

    <!-- Modal video -->
    <div class="modal fade video-modal productVideo"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog  modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">  
                    <h5 class="modal-title">Flip Off Seals</h5>             
                    <button id="close" type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span class="icon-close icomoon"></span>
                    </button>
                </div>
                <div class="modal-body">                

                    <iframe id="ytplayer" class="modalvideo" width="100%" src="https://www.youtube.com/embed/yavBGCMowbI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
    <!--/ modal video -->

    <script>   
        $('.video-modal').on('hidden.bs.modal', function (e) {
            $('.video-modal iframe').attr('src', '');
        });

        $('.video-modal').on('show.bs.modal', function (e) {
            $('.video-modal iframe').attr('src', 'https://www.youtube.com/embed/yavBGCMowbI?rel=0&amp;autoplay=1');
        });
    </script>


<!-- request quotation -->
<div class="offcanvas offcanvas-end" tabindex="-1" id="requestQuotation" aria-labelledby="offcanvasRightLabel">
  <div class="offcanvas-header">
    <h5 id="offcanvasRightLabel">Request for Quotation</h5>
    <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
  </div>
  <div class="offcanvas-body">
    <div class="careerRightSection">
        <?php include 'includes/requestquotation.php'?>
        <p class="pt-3">
            <span class="fsbold">Note:</span> After submit your request, Our Team will connect you soon.
        </p>
        </div>
    </div>
</div>
<!--/ request quotation -->

<!-- request sample -->
<div class="offcanvas offcanvas-end" tabindex="-1" id="requestSample" aria-labelledby="offcanvasRightLabel">
  <div class="offcanvas-header">
    <h5 id="offcanvasRightLabel">Request Samples</h5>
    <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
  </div>
  <div class="offcanvas-body">
    <div class="careerRightSection">
         <?php include 'includes/requestsampleform.php'?>
        <p class="pt-3">
            <span class="fsbold">Note:</span> After submit your request, Our Team will connect you soon.
        </p>
        </div>
    </div>
</div>
<!--/ request sample -->


</body>

</html>