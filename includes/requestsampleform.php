<form class="form customForm bg-transparent p-0 shadow-none">
            <div class="form-group p-0">
                <label>Full Name</label>
                <div class="input-group">
                    <input type="text" class="form-control" name="name" placeholder="Write Your Name">
                </div>
            </div>
            <div class="form-group p-0">
                <label>Phone Number </label>
                <div class="input-group">
                    <input type="text" class="form-control" name="phone" placeholder="Enter Valid Phone" >
                </div>
            </div>
            <div class="form-group p-0">
                <label>Email </label>
                <div class="input-group">
                    <input type="text" class="form-control" name="phone" placeholder="Enter Email" >
                </div>
            </div>
            <div class="form-group p-0">
                <label>Company Name (Optional) </label>
                <div class="input-group">
                    <input type="text" class="form-control" name="phone" placeholder="Company Name" >
                </div>
            </div> 
            <div class="form-group p-0">
                <label>Write Message (Optional) </label>
                <div class="input-group">
                    <textarea class="form-control" style="height:100px;">

                    </textarea>
                </div>
            </div>                 
            <button class="btn btn-info w-100 text-uppercase" name="submitContact">Send Request</button>          
        </form>