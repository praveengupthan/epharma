

<section class="masterSlider videSlider">
    <div class="example-marquee">
        <div id="ytbg2" data-ytbg-play-button="true"  data-youtube="https://www.youtube.com/watch?v=GlEnmQkik1Q"></div>
        <div class="content">
            <div class="inner">        
            </div>
        </div>
    </div>

    <div class="swiper-container bannerSwiper">
        <div class="swiper-wrapper">
            <div class="swiper-slide text-center">
                <h4>One stop shop -your primary & secondary packaging expert </h4>                
            </div>
            <div class="swiper-slide text-center">
                <h4>State-of-the-art infrastructure with global standards </h4>
            </div>
            <div class="swiper-slide text-center">
                <h4>ISO 7 & 8 cleanroom Manufacturing </h4>
            </div>      
        </div>
    </div>   
</section>




