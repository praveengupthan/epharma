<div id="load"></div>
 <!-- header -->
    <header class="fixed-top" id="nav">
        <div class="custom-container">
        <div class="navbar navbar-expand-lg bsnav">
          <a class="navbar-brand" href="index.php">
              <img src="img/logo.png" alt="">
          </a>
            <button class="navbar-toggler toggler-spring"><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse justify-content-md-between">
              <ul class="navbar-nav navbar-mobile ms-auto">
                <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='index.php'){echo'active';}else {echo'nav-link';}?> text-uppercase fsbold" href="index.php">Home</a></li>              
                <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='about.php'){echo'active';}else {echo'nav-link';}?> text-uppercase fsbold" href="about.php">About</a></li>   
                <li class="nav-item dropdown megamenu"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='products.php'){echo'active';}else {echo'nav-link';}?> text-uppercase fsbold" href="products.php">Products <i class="caret"></i></a>
                  <div class="navbar-nav">
                    <div class="container">                      
                      <!-- products row -->
                      <div class="row">
                          <div class="col-lg-4">                            
                              <a href="safeseals.php" class="header-product-col d-flex">
                                  <img src="img/products/safesealsthumb.jpg" alt="">
                                  <div>
                                      <h6>Safe Seals</h6>
                                      <p>We manufacture 13mm, 20mm, 28mm, 32mm and printed flip off Safe Seals.</p>
                                  </div>
                              </a>
                          </div>
                          <div class="col-lg-4">                            
                              <a href="droppers.php" class="header-product-col d-flex">
                                  <img src="img/products/droppersthumb.jpg" alt="">
                                  <div>
                                      <h6>Droppers</h6>
                                      <p>Our Flip Free Technology ensures the smooth and burr free surface after Flip-Off.</p>
                                  </div>
                              </a>
                          </div>
                          <div class="col-lg-4">                            
                              <a href="ophthalmics.php" class="header-product-col d-flex">
                                  <img src="img/products/opthalmicsthumb.jpg" alt="">
                                  <div>
                                      <h6>Ophthalmics</h6>
                                      <p>Due to our enormous understanding and massive knowledge of this business, East Pharma involved ..</p>
                                  </div>
                              </a>
                          </div>
                          <div class="col-lg-4">                            
                              <a href="inductionsealingwads.php" class="header-product-col d-flex">
                                  <img src="img/products/inductionsealingwadsthumb.jpg" alt="">
                                  <div>
                                      <h6>Induction sealing WADS</h6>
                                      <p>Flip-Off seals are ready-to-use seals help pharmaceutical and biopharmaceutical....</p>
                                  </div>
                              </a>
                          </div>
                          <div class="col-lg-4">                            
                              <a href="measuringcupsandspoon.php" class="header-product-col d-flex">
                                  <img src="img/products/measuringcupsandspoonsthumb.jpg" alt="">
                                  <div>
                                      <h6>Measuring Cups And Spoon</h6>
                                      <p>All our Ophthalmic products will stringently comply with pharmaceutical ..</p>
                                  </div>
                              </a>
                          </div>                        
                          <div class="col-lg-4">                            
                              <a href="printing.php" class="header-product-col d-flex">
                                  <img src="img/products/tyvekprintingthumb.jpg" alt="">
                                  <div>
                                      <h6>Printing</h6>
                                      <p>East Pharma Technologies offers various printing technologies under one roof for ...</p>
                                  </div>
                              </a>
                          </div>
                          <div class="col-lg-4">                            
                              <a href="sterilepouches.php" class="header-product-col d-flex">
                                  <img src="img/products/sterilepouchesthumb.jpg" alt="">
                                  <div>
                                      <h6>Sterile Pouches </h6>
                                      <p>We are a top-notch manufacturer of a wide spectrum of Assembly Droppers that is...</p>
                                  </div>
                              </a>
                          </div>  
                           <div class="col-lg-4">                            
                              <a href="roppcap.php" class="header-product-col d-flex">
                                  <img src="img/products/roppcapthumb.jpg" alt="">
                                  <div>
                                      <h6>ROPP Cap </h6>
                                      <p>The aluminium Ropp caps have gained tremendous importance in the ...</p>
                                  </div>
                              </a>
                          </div>   
                          <div class="col-lg-4">                            
                              <a href="masterbatch.php" class="header-product-col d-flex">
                                  <img src="img/products/masterbatchthumb.jpg" alt="">
                                  <div>
                                      <h6>Master Batch </h6>
                                      <p>Master Batch/ Colorant manufactured in East Pharma Technologies in clean room....</p>
                                  </div>
                              </a>
                          </div>                    
                      </div>
                      <!-- products row -->
                      <div class="row justify-content-center py-3">
                        <div class="col-lg-4">
                                <a href="products.php" class="customBtn d-block w-100 text-center">View All Products</a>
                        </div>
                      </div>
                   
                    </div>
                  </div>
                </li>
                <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='facilities.php'){echo'active';}else {echo'nav-link';}?> text-uppercase fsbold" href="facilities.php">Facilities</a></li>
                <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='qualitypolicy.php'){echo'active';}else {echo'nav-link';}?> text-uppercase fsbold" href="qualitypolicy.php">Quality</a></li>
                <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='career.php'){echo'active';}else {echo'nav-link';}?> text-uppercase fsbold" href="career.php">Career</a></li>
              </ul>

              <ul class="navbar-nav rightnav navbar-mobile ms-auto">
                <li class="nav-item"><a class="nav-link text-uppercase fsbold btn customBtn" href="contact.php">Contact</a></li>
              </ul> 
            </div>
          </div>
          <div class="bsnav-mobile">
            <div class="bsnav-mobile-overlay"></div>
            <div class="navbar"></div>
        </div>
        </div>
    </header>
    <!--/ header -->