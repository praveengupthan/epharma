<!-- scripts files -->
<script src="js/jquery-3.6.0.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/swiper.min.js"></script>
<script src="js/bsnav.js"></script>
<script src="js/baguetteBox.js"></script>
<script src="js/glightbox.min.js"></script>
<script src="js/main.js"></script>
<!--[if lte IE 9]>
    <script src="js/ie.lteIE9.js"></script>
    <![endif]-->
<script src="js/aos.js"></script> 
<script>
    AOS.init({
        easing: 'ease-in-out-sine'
    });
</script>
<script src="js/jquery.youtube-background.js?v=1.0.8"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
            jQuery('[data-youtube], [data-vbg]').youtube_background();
    });
    document.querySelector('#ytbg2').addEventListener('video-background-pause', function(){
        console.log(arguments);
    });
</script>
<script src="js/custom.js"></script>

<script>
    var swiper = new Swiper('.bannerSwiper', {
    slidesPerView: 1,
    spaceBetween: 30,
    loop: true,
    autoplay: {
        delay: 5000,
        disableOnInteraction: true,
        },
    });
</script>

<!-- main swiper js -->
<script>
    var mainSlider = new Swiper(".mySwiper2", {
        parallax: true,
        speed: 2000,
        effect: "slide",
        direction: "vertical",
        autoplay: true,
        loop: true,
        navigation: {
            nextEl: ".upk-button-next",
            prevEl: ".upk-button-prev"
        },
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
            renderBullet: function (index, className) {
                return (
                    '<span class="' +
                    className +
                    ' swiper-pagination-bullet--svg-animation"><svg width="28" height="28" viewBox="0 0 28 28"><circle class="svg__circle" cx="14" cy="14" r="10" fill="none" stroke-width="2"></circle><circle class="svg__circle-inner" cx="14" cy="14" r="2" stroke-width="3"></circle></svg></span>'
                );
            }
        }
    });

    //home page rpdocuts slider
    var swiper = new Swiper('.productsSlider', {
        slidesPerView: 1,
        spaceBetween: 3,
        autoplay: {
            delay: 5000,
            disableOnInteraction: true,
        },
        // init: false,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            480: {
                slidesPerView: 2,
                spaceBetween: 3,
            },
            640: {
                slidesPerView: 3,
                spaceBetween: 3,
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 3,
            },
            1024: {
                slidesPerView: 7,
                spaceBetween: 5,
            },
        }
    });
</script>

<script>
    //home page rpdocuts slider
    var swiper = new Swiper('.productsSliderProductDetail', {
        slidesPerView: 1,
        spaceBetween: 3,
        autoplay: {
            delay: 5000,
            disableOnInteraction: true,
        },
        // init: false,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            480: {
                slidesPerView: 2,
                spaceBetween: 3,
            },
            640: {
                slidesPerView: 3,
                spaceBetween: 3,
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 3,
            },
            1024: {
                slidesPerView: 3,
                spaceBetween: 5,
            },
        }
    });
</script>





<script>
    baguetteBox.run('.grid-gallery', { animation: 'slideIn'});
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
<script>
    baguetteBox.run('.compact-gallery', { animation: 'slideIn'});
</script>

<script>    
  //contact form validatin
  $('#contact_form').validate({
    ignore: [],
    errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
    errorElement: 'div',
    errorPlacement: function (error, e) {
        e.parents('.form-group').append(error);
    },
    highlight: function (e) {
        $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
        $(e).closest('.text-danger').remove();
    },  
    rules: {
        name: {
            required: true                
        },
        phone: {
            required: true,    
            number: true,
            minlength: 10,
            maxlength:10                   
        },  
        email:{
          required:true,
          email: true
        },
        howDid:{
          required:true          
        },
        sub:{
          required:true,
        }  
    },

    messages: {
        name: {
            required: "Enter Name"
        },
        phone: {
            required: "Enter Valid Mobile Number"                  
        }, 
        email:{
          required: "Enter Valid Email"        
        },
        email:{
          required: "How did you know abut us"        
        },
        sub:{
          required: "Enter Subject"        
        }            
    },
});
</script>


