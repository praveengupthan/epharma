<footer class="position-relative" id="footer">
    <!-- top footer -->
    <div class="topFooter py-5">
        <!--container -->
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <h3>Contact us</h3>
                    <p>East pharma technologies, SDF block-1, Ground Floor, EPIP, TSIIC, Patancheru (Mdl),
                        Sangareddy (D), Hyderabad, Telangana - 502307, India.</p>
                    <p>info@eastpharmatechnologies.com</p>
                    <p>(+91) 9000004304, (+91) 9440919427</p>
                    <!-- footer social -->
                    <div class="footerSocial">
                        <a href="https://www.facebook.com/" target="_blank"><span class="icon-facebook icomoon"></span></a>
                        <a href="https://twitter.com/?lang=en" target="_blank"><span class="icon-twitter icomoon"></span></a>
                        <a href="https://www.linkedin.com/feed/" target="_blank"><span class="icon-linkedin icomoon"></span></a>
                    </div>
                    <!--/ footer social -->
                </div>
                <div class="col-lg-2 col-md-6">
                    <h3>Quick Links</h3>
                    <ul class="footerLinks m-0 ps-0 pe-3">
                        <li><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='index.php'){echo'active';}else {echo'nav-link';}?>" href="<?php echo $homeLink?>">Home</a></li>
                        <li><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='about.php'){echo'active';}else {echo'nav-link';}?>" href="about.php">About</a></li>
                        <li><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='facilities.php'){echo'active';}else {echo'nav-link';}?>" href="facilities.php">Facilities</a></li>
                        <li><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='qualitypolicy.php'){echo'active';}else {echo'nav-link';}?>" href="qualitypolicy.php">Quality Policy</a></li>
                        <li><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='career.php'){echo'active';}else {echo'nav-link';}?>" href="career.php">Careers</a></li>
                        <li><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='contact.php'){echo'active';}else {echo'nav-link';}?>" href="contact.php">Contact</a></li>
                    </ul>
                </div>
                <div class="col-lg-7 col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Products</h3>
                        </div>
                        <div class="col-md-6">
                            <ul class="footerLinks m-0 ps-0 pe-3">
                                <li><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='safeseals.php'){echo'active';}else {echo'nav-link';}?>" href="safeseals.php">Safe Seals</a></li>
                                <li><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='droppers.php'){echo'active';}else {echo'nav-link';}?>" href="droppers.php">Droppers </a></li>
                                <li><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='ophthalmics.php'){echo'active';}else {echo'nav-link';}?>" href="ophthalmics.php">Ophthalmic</a></li>
                                <li><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='inductionsealingwads.php'){echo'active';}else {echo'nav-link';}?>" href="inductionsealingwads.php">Induction sealing WADS</a></li>
                                <li><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='measuringcupsandspoon.php'){echo'active';}else {echo'nav-link';}?>" href="measuringcupsandspoon.php">Measuring Cup & Spoons</a></li>
                                <li><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='printing.php'){echo'active';}else {echo'nav-link';}?>" href="printing.php">Printing</a></li>
                                <li><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='sterilepouches.php'){echo'active';}else {echo'nav-link';}?>" href="sterilepouches.php">Sterile Pouches </a></li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <ul class="footerLinks m-0 ps-0 pe-3">
                                <li><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='roppcap.php'){echo'active';}else {echo'nav-link';}?>" href="roppcap.php"> ROPP Cap</a></li>
                                <li><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='masterbatch.php'){echo'active';}else {echo'nav-link';}?>" href="masterbatch.php">Master Batch  </a></li>
                                <li><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='pharmatubing.php'){echo'active';}else {echo'nav-link';}?>" href="pharmatubing.php">Pharma Tubing</a></li>
                                <li><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='reinforcedhoses.php'){echo'active';}else {echo'nav-link';}?>" href="reinforcedhoses.php">Reinforced Hoses</a></li>
                                <li><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='inflatableseals.php'){echo'active';}else {echo'nav-link';}?>" href="inflatableseals.php">Inflatable seals</a></li>
                                <li><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='siliconemoldedsiftersieves.php'){echo'active';}else {echo'nav-link';}?>" href="siliconemoldedsiftersieves.php">Silicone Molded sifter Sieves</a></li>
                                <li><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='stripseals.php'){echo'active';}else {echo'nav-link';}?>" href="stripseals.php">Strip Seals</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/ container -->
    </div>
    <!--/ top footer -->
    <!-- bottom footer -->
    <div class="bottomFooter">
        <p>Copyright © 2017 East Pharma Technologies</p>
    </div>
    <!--/ bottom footer -->
    <a href="javascript:void(0)" class="moveTop" id="movetop"><span class="icon-arrowup icomoon"></span></a>
</footer>