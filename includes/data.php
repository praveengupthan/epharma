<!-- variables -->
<?php 
    $SPageHome = 'Home';    
    $productsBrCrumbName = 'Products';
    $productsLink = 'products.php';
    $aboutTitle ='About us';
    $aboutMainTitle = 'Want to know more about us';
    $homeAboutDesc = 'We at east pharma technologies, are committed to enhance customer satisfaction by manufacturing and supply of high quality packaging products through implementation and continual improvement of quality management system.';
?>


<!-- loops -->
<?php 
//home page products
$productSliderItem=array(
    array(
        "safeseals.php",
        "safesealsthumb",
        "Safe Seals",
        "We have various type of Adaptors which are manufactured in clean room ISO class 8.",        
    ),
    array(
        "droppers.php",
        "droppersthumb",
        "Droppers",
        "Base Caps manufactured in East Pharma Technologies under clean room condition ...",        
    ),
    array(
        "opthalmicsbottles.php",
        "opthalmicsthumb",
        "Ophthalmics",
        "Ophthalmic 3 PCs Components manufactured in East Pharma Technologies in...",        
    ),
    array(
        "inductionsealingwads.php",
        "inductionsealingwadsthumb",
        "Induction sealing wads",
        "We have unique combinations of various loading of TiO2 with various base polymers,...",        
    ),
    array(
        "measuringcupsandspoon.php",
        "measuringcupsandspoonsthumb",
        "Measuring Cup and Spoon",
        "Measuring Cup and Spoon manufactured in East Pharma Technologies in clean..",        
    ),    

    array(
        "tyvekprinting.php",
        "tyvekprintingthumb",
        "Printing",
        "We have unique combinations of various loading of TiO2 with various base polymers,...",        
    ),
    array(
        "sterilepouches.php",
        "sterilepouchesthumb",
        "Sterile Pouches ",
        "We have unique combinations of various loading of TiO2 with various base polymers,...",        
    ),
    array(
        "roppcap.php",
        "roppcapthumb",
        "ROPP Cap ",
        "We have unique combinations of various loading of TiO2 with various base polymers,...",        
    ),
    array(
        "masterbatch.php",
        "masterbatchthumb",
        "Master Batch",
        "Master Batch/ Colorant manufactured in East Pharma Technologies in clean room for ...",        
    ),  
    array(
        "pharmatubing.php",
        "pharmatubingthumb",
        "Pharma Tubing",
        "Base Caps manufactured in East Pharma Technologies under clean room..",        
    ), 
    array(
        "reinforcedhoses.php",
        "reinforcedhosesthumb",
        "Reinforced Hoses",
        "Base Caps manufactured in East Pharma Technologies under clean room..",        
    ), 
    array(
        "inflatableseals.php",
        "inflatablesealsthumb",
        "Inflatable seals",
        "Base Caps manufactured in East Pharma Technologies under clean room..",        
    ), 
    array(
        "siliconemoldedsiftersieves.php",
        "siliconemoldedsiftersievesthumb",
        "Silicone Molded sifter Sievesoforming",
        "Effective packing solutions contribute to the effective format of drug delivery in the...",        
    ),
    array(
        "stripseals.php",
        "stripsealsthumb",
        "Strip Seals",
        "Strip Seals manufactured for Tamper Proof Applications...",        
    ),   
);

//quality policy 

$qualityPolicyItem=array(
    array(
        "icon-progress-chart",
        "Increase in Productivity",
    ),
     array(
        "icon-settings",
        "Upgrading of Technology",
    ),
     array(
        "icon-customerservice",
        "Responding to customer Quaries",
    ),
     array(
        "icon-training",
        "Training & Retraining of employees",
    ),
     array(
        "icon-garbage",
        "Reducing scrap & Waste",
    ),
     array(
        "icon-fast-delivery",
        "Strict Adherence to delivery Schedule",
    ),
);

//home strengrth item 
$homeStrengthItem=array(
    array(
        "icon-rocket",
        "State-of-the-art facility – highly sophisticated equipment installed for precision and durability. .",
    ),
     array(
        "icon-globe",
        "Only company in India using fully hot runner molds for Packaging components having world class innovative technology which ensures highest accuracy and consistent quality",
    ),
     array(
        "icon-success",
        "Our core team is qualified with more than 20 years of experience in Pharma Packaging",
    ),
     array(
        "icon-high-speed-train",
        "High speed Six camera inspection systems for flawless products delivery.",
    ),
     array(
        "icon-portfolio",
        "Huge product portfolio",
    ),
     array(
        "icon-research",       
        "A continual Research & Development",
    ),
);

//product item 
$productItem=array(

    array(
        "safeseals.php",
        "safesealsbanner.jpg",
        "Safe Seals",
        "Safe Seals manufactured in East Pharma Technologies in clean room for vials....."
    ),
    array(
        "droppers.php",
        "droppersbanner.jpg",
        "Droppers",
        "Droppers manufactured in East Pharma Technologies in clean room ISO class 8...."
    ),
    array(
        "ophthalmics.php",
        "opthalmicsbanner.jpg",
        "Ophthalmics",
        "Ophthalmic 3 PCs Components manufactured in East Pharma Technologies in clean room...."
    ),
    array(
        "inductionsealingwads.php",
        "inductionsealingwadsbanner.jpg",
        "Induction sealing wads",
        "Induction Sealing wads/Liners for Closures for Pharmaceutical Applications....."
    ),
    array(
        "measuringcupsandspoon.php",
        "measuringcupsandspoonsbanner.jpg",
        "Measuring Cups And Spoon",
        "Measuring Cup and Spoon manufactured in East Pharma Technologies in clean room for...."
    ),   
     array(
        "printing.php",
        "tyvekprintingbanner.jpg",
        "Printing",
        "East Pharma Technologies offers various printing technologies under one roof for...."
    ),
    array(
        "sterilepouches.php",
        "sterilepouchesbanner.jpg",
        "Sterile Pouches ",
        "A specially designed medical grade and heavy-duty paper and film laminate, ...."
    ),
    array(
        "roppcap.php",
        "roppcapbanner.jpg",
        "ROPP Cap ",
        "The aluminium Ropp caps have gained tremendous importance in the pharma ...."
    ),
    array(
        "masterbatch.php",
        "masterbatchbanner.jpg",
        "Master Batch",
        "Master Batch/ Colorant manufactured in East Pharma Technologies in clean room...."
    ),
    array(
        "pharmatubing.php",
        "pharmatubingbanner.jpg",
        "Pharma Tubing",
        "FEP-Pure 55 and EP-Pure 65 are platinum cured, pharmaceutical and biopharmaceutical...."
    ),
    array(
        "reinforcedhoses.php",
        "reinforcedhosesbanner.jpg",
        "Reinforced Hoses",
        "EP-Fit is platinum cured silicone hose reinforced with polyester braiding....."
    ),
    array(
        "inflatableseals.php",
        "inflatablesealsbanner.jpg",
        "Inflatable seals",
        "EP-Seal is tubular elastomer seals which is activated by internal ...."
    ),
    array(
        "siliconemoldedsiftersieves.php",
        "siliconemoldedsiftersievesbanner.jpg",
        "Silicone Molded sifter Sieves",
        "Sieving is a simple technique for separating particles of different sizes and Silicone Moulded...."
    ), 
    array(
        "stripseals.php",
        "stripsealsbanner.jpg",
        "Strip Seals",
        "Strip Seals manufactured for Tamper Proof Applications...."
    ),   
);

?>
