<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Reinforced Hoses</title>    
    <?php include 'includes/styles.php'?>
    <?php include 'includes/data.php'?>
</head>

<body>
    <?php include 'includes/header.php'?>
     <main class="subPage">
         <!-- sub page header -->
         <div class="subPageHeader productDetailHeader">
             <div class="container">
                 <div class="row">
                     <div class="col-lg-6 leftsubpageHeader align-self-center text-center">
                        <figure>
                            <a href="javascript:void(0)" class="customBtn" data-bs-toggle="modal" data-bs-target=".productVideo"><span class="icon-play-button"></span> </a>
                            <img src="img/products/reinforcedhosesbanner.jpg" alt="" class="img-fluid">
                        </figure>                        
                     </div>
                     <div class="col-lg-6 align-self-center">
                         <article class="p-3">
                            <h1 class="h3 fsbold">Reinforced Hoses</h1>
                            <p>EP-Fit is platinum cured silicone hose reinforced with polyester braiding. It has excellent flexibility for pressurized fluid transfer application in pharmaceutical, biotech industries and food application.  </p>
                            <a href="javascript:void(0)" class="customBtn" data-bs-toggle="offcanvas" data-bs-target="#requestQuotation" aria-controls="offcanvasRight">Request for Quotation</a>    
                            <a href="javascript:void(0)" class="customBtn" data-bs-toggle="offcanvas" data-bs-target="#requestSample" aria-controls="offcanvasRight">Request Samples</a> 
                            <p class="pt-2"> <a class="link" href="products.php"><span class="icon-arrowleft icomoon"></span> Back to Products </a></p>
                         </article>
                     </div>
                 </div>
             </div>
         </div>
         <!--/ sub page header -->

         <!-- sub page body -->
         <div class="subpageBody pt-0">
            <div class="container">                
            <header id="header" class="header" data-scrollto-offset="0">
            <!-- product detail nav -->
            <nav class="navbar navbar-productdetail mt-md-2">              
                <ul class="d-md-flex justify-content-md-between align-items-center" id="mobileItems">
                    <li><a class="nav-link scrollto" href="flipoffsealsnew.php#overview">Overview</a></li>
                    <li><a class="nav-link scrollto" href="flipoffsealsnew.php#productAttributes">Sizes</a></li>
                    <li><a class="nav-link scrollto" href="flipoffsealsnew.php#downloadableResources">Downloads</a></li>
                    <li><a class="nav-link scrollto" href="flipoffsealsnew.php#certifications">Certifications</a></li>                    
                    <li><a class="nav-link scrollto" href="flipoffsealsnew.php#relatedProducts">Related Products</a></li>
                </ul>
            </nav>
            <!--/ product detail nav -->
            </header>           
             </div>
             <!-- product detail description -->
             <section class="featured-services pt-3" id="overview">
              <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="h4 fsbold border-bottom pb-2 mb-3">Overview</h2>
                        <p>EP-Fit Hose is not intended for implantation and is not to be used for continuous steam application. </p>
                    </div>                                     
                    <div class="col-md-12 align-self-center">  
                        <h6 class="fsbold d-inline-block">Key Features </h6>
                        <ul class="list-items">
                            <li>Excellent Flexibility.</li>
                            <li>Manufactured from low volatile grade silicone resin.</li>
                            <li>Imparts no taste and odor</li>
                            <li>Hardness value of 65-70 Shore A .</li>
                            <li>US FDA (Foods and Drugs Administration) Standard 21 CFR 177.2600 </li>
                            <li>German BfR Standard part XV</li>
                            <li>USP Class VI standard</li>
                            <li>ISO 10993</li>
                            <li>RoHS</li>
                            <li>Free of Phthalate/Bisphenol/Volatile Plasticizer</li>
                            <li>TSE/BSE </li>
                            <li>Custom lengths, color coding, 316L stainless steel sanitary fittings, hose assemblies and/or packaging available.</li>
                            <li>Sterilizable by Autoclave, Ethylene Oxide Gas and Gamma Radiation.</li>
                        </ul>
                    </div>
                    <div class="col-md-12 align-self-center">  
                        <h6 class="fsbold d-inline-block">Applications</h6>
                        <ul class="list-items">
                            <li>Pharmaceutical and Biotech material transfer.</li>
                            <li>Critical Liquid Transfer</li>
                            <li>Load Cell </li>
                            <li>Analytical Lab Food and Beverages.</li>
                            <li>Biotechnology </li>
                            <li>Laboratory use</li>                           
                        </ul>
                    </div>
                    <div class="col-md-12">   
                        <h4 class="pb-2 h4 fbold">Reinforced Hoses Gallery</h4>
                        <!-- gallery -->
                        <section class="gallery-block grid-gallery mt-0 pt-0 aos-item pb-0" data-aos="fade-up">
                            <div class="row g-2">
                                <div class="col-md-6 col-lg-3 col-6 item ">
                                    <a class="lightbox" href="img/products/reinforced/reinforced01.jpg">
                                        <img class="img-fluid image scale-on-hover" src="img/products/reinforced/reinforced01.jpg">
                                    </a>
                                </div>
                                <div class="col-md-6 col-lg-3 col-6 item">
                                    <a class="lightbox" href="img/products/reinforced/reinforced02.jpg">
                                        <img class="img-fluid image scale-on-hover" src="img/products/reinforced/reinforced02.jpg">
                                    </a>
                                </div>
                                <div class="col-md-6 col-lg-3 col-6 item">
                                    <a class="lightbox" href="img/products/reinforced/reinforced03.jpg">
                                        <img class="img-fluid image scale-on-hover" src="img/products/reinforced/reinforced03.jpg">
                                    </a>
                                </div>                               
                                <div class="col-md-6 col-lg-3 col-6 item">
                                    <a class="lightbox" href="img/products/reinforced/reinforced04.jpg">
                                        <img class="img-fluid image scale-on-hover" src="img/products/reinforced/reinforced04.jpg">
                                    </a>
                                </div>                                                          
                            </div>                           
                        </section>                 
                        <!--/ gallery -->
                    </div>
                </div>
                </div>
             </section>
              <section class="featured-services" id="productAttributes">
              <div class="container">
                  <div class="row justify-content-center">
                   <div class="col-md-12">
                    <h3 class="h4 fsbold border-bottom pb-2 mb-3">Available Sizes</h2>
                   </div>
                    <div class="col-md-12 text-center">                      
                        <div class="table-responsive pb-4">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>                                   
                                        <th scope="col" class="text-start">Size in mm (IDXOD)</th>
                                        <th scope="col">Size in Inch (ID) </th>
                                        <th scope="col">Max Working Pressure (Bar)</th>
                                        <th scope="col">Min Burst Pressure (Bar) </th>
                                        <th scope="col"> Min Bend Radius (mm) </th>
                                        <th scope="col">Standard Length (Meter) </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>                                      
                                        <td class="text-start">6 X 13</td>
                                        <td>1/4"</td>
                                        <td>10.0</td>
                                        <td>30</td>
                                        <td>50</td>
                                        <td>15</td>
                                    </tr>
                                    <tr>                                      
                                        <td class="text-start">9 X 16</td>
                                        <td>11/32"</td>
                                        <td>9.0</td>
                                        <td>27</td>
                                        <td>50</td>
                                        <td>15</td>
                                    </tr>
                                    <tr>                                      
                                        <td class="text-start">12 X 20</td>
                                        <td>1/2"</td>
                                        <td>7.0</td>
                                        <td>21</td>
                                        <td>80</td>
                                        <td>15</td>
                                    </tr>
                                    <tr>                                      
                                        <td class="text-start">16 X 24</td>
                                        <td>5/8"</td>
                                        <td>7.0</td>
                                        <td>21</td>
                                        <td>90</td>
                                        <td>15</td>
                                    </tr>
                                    <tr>                                      
                                        <td class="text-start">19 X 27</td>
                                        <td>3/4"</td>
                                        <td>6.0</td>
                                        <td>18</td>
                                        <td>110</td>
                                        <td>15</td>
                                    </tr>
                                    <tr>                                      
                                        <td class="text-start">25 X 35</td>
                                        <td>1"/td>
                                        <td>4.0</td>
                                        <td>12</td>
                                        <td>160</td>
                                        <td>15</td>
                                    </tr>                                   
                                </tbody>
                            </table>
                            <p>*Tested at Standard Temperature Condition. Please reduce pressure values by 20% for each increase of -100°C / -212°F</p>
                            <p>**We also manufactured customized diameters till 1”. CP-Consult Plant. </p>
                        </div>
                    </div>
                  </div>
                </div>
              
              </section>
             
              <section class="featured-services" id="downloadableResources">
              <div class="container">
                  <div class="row justify-content-center">
                        <div class="col-md-5">
                            <h2 class="text-center pb-4">Downloadable Resources</h2>
                            <div class="d-none">                           
                            <a href="img/dummy.pdf" download class="downloadItem"><span class="icon-download"></span> Downloadable Component Name will be here</a>
                            <a href="img/dummy.pdf" download  class="downloadItem"><span class="icon-download"></span> Downloadable Component Name will be here</a>
                            </div>
                            <p class="text-center h5 fwhite">Coming soon</p>
                      </div>
                  </div>
              </div>r
              </section>
              <section class="featured-services" id="certifications">
                <div class="container">
                  <div class="row justify-content-center">
                    <div class="col-md-12">
                        <h3 class="h4 fsbold border-bottom pb-2 mb-3">Certifications</h2>
                    </div>
                  </div>
                 

                    <!-- certifications -->
                    <div class="certificateImages py-3 d-flex justify-content-between">
                        <?php include 'includes/certificates.php' ?>
                    </div>
                    <!--/ certifications -->
                </div>
               
              </section>
              <section class="featured-services realtedProducts pt-0" id="relatedProducts">
              <div class="container">                 
                  <h3 class="h4 fsbold pb-2 mb-3">Related Products</h2>

                <div class="swiper-container w-100 productsSliderProductDetail">
                <div class="swiper-wrapper">
                    <!-- swiper slider -->
                    <?php
                    for($i=0; $i<count($productSliderItem);$i++){ ?>
                    <div class="swiper-slide">
                         <div class="card productCard mb-3">
                            <a href="<?php echo $productItem[$i][0]?>">
                                <img src="img/products/<?php echo $productItem[$i][1]?>" alt="" class="img-fluid">
                            </a>
                            <div class="card-body">
                                <h5 class="card-title"><?php echo $productItem[$i][2]?></h5>
                                <p class="card-text"><?php echo $productItem[$i][3]?></p>
                                <a href="<?php echo $productItem[$i][0]?>" class="card-link">Read More &rarr; </a>
                            </div>
                        </div>
                    </div>   
                   <?php } ?>
                    <!--/ swiper slider -->                 
                </div>
                <!-- Add Arrows -->
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
            </div>
              </div>
              </section>
             <!-- product detail description -->
         </div>
         <!--/ sub page body -->
     </main>
    <!-- footer -->
     <?php include 'includes/footer.php'?>
    <!--/ footer -->
     <?php include 'includes/scripts.php'?>

     <script>
        $(document).ready(function(){
            $("#DetailDropdown").click(function(){
                $("#mobileItems").toggle();
            });
        });
    </script>

     <script>
        window.onscroll = function() {myFunction()};
        var navbar = document.getElementById("header");
        var sticky = navbar.offsetTop;
        function myFunction() {
        if (window.pageYOffset >= sticky) {
            navbar.classList.add("stickynav")
        } else {
            navbar.classList.remove("stickynav");
        }
        }
        </script>

    <!-- Modal video -->
    <div class="modal fade video-modal productVideo"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog  modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">  
                    <h5 class="modal-title">Flip Off Seals</h5>             
                    <button id="close" type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span class="icon-close icomoon"></span>
                    </button>
                </div>
                <div class="modal-body">                

                    <iframe id="ytplayer" class="modalvideo" width="100%" src="https://www.youtube.com/embed/v_I3Dpay90w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
    <!--/ modal video -->

    <script>   
        $('.video-modal').on('hidden.bs.modal', function (e) {
            $('.video-modal iframe').attr('src', '');
        });

        $('.video-modal').on('show.bs.modal', function (e) {
            $('.video-modal iframe').attr('src', 'https://www.youtube.com/embed/v_I3Dpay90w?rel=0&amp;autoplay=1');
        });
    </script>


<!-- request quotation -->
<div class="offcanvas offcanvas-end" tabindex="-1" id="requestQuotation" aria-labelledby="offcanvasRightLabel">
  <div class="offcanvas-header">
    <h5 id="offcanvasRightLabel">Request for Quotation</h5>
    <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
  </div>
  <div class="offcanvas-body">
    <div class="careerRightSection">
        <?php include 'includes/requestquotation.php'?>
        <p class="pt-3">
            <span class="fsbold">Note:</span> After submit your request, Our Team will connect you soon.
        </p>
        </div>
    </div>
</div>
<!--/ request quotation -->

<!-- request sample -->
<div class="offcanvas offcanvas-end" tabindex="-1" id="requestSample" aria-labelledby="offcanvasRightLabel">
  <div class="offcanvas-header">
    <h5 id="offcanvasRightLabel">Request Samples</h5>
    <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
  </div>
  <div class="offcanvas-body">
    <div class="careerRightSection">
         <?php include 'includes/requestsampleform.php'?>
        <p class="pt-3">
            <span class="fsbold">Note:</span> After submit your request, Our Team will connect you soon.
        </p>
        </div>
    </div>
</div>
<!--/ request sample -->


</body>

</html>