<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sterile Pouches</title>    
    <?php include 'includes/styles.php'?>
    <?php include 'includes/data.php'?>
</head>

<body>
    <?php include 'includes/header.php'?>
     <main class="subPage">
         <!-- sub page header -->
         <div class="subPageHeader productDetailHeader">
             <div class="container">
                 <div class="row">
                     <div class="col-lg-6 leftsubpageHeader align-self-center text-center">
                        <figure>
                            <a href="javascript:void(0)" class="customBtn" data-bs-toggle="modal" data-bs-target=".productVideo"><span class="icon-play-button"></span> </a>
                            <img src="img/products/sterilepouchesbanner.jpg" alt="" class="img-fluid">
                        </figure>                        
                     </div>
                     <div class="col-lg-6 align-self-center">
                         <article class="p-3">
                            <h1 class="h3 fsbold">Sterile Pouches</h1>
                            <p>A specially designed medical grade and heavy-duty paper and film laminate, Easy peel able without delamination, Can be customized as per Client requirement and Applications </p>
                            <a href="javascript:void(0)" class="customBtn" data-bs-toggle="offcanvas" data-bs-target="#requestQuotation" aria-controls="offcanvasRight">Request for Quotation</a>    
                            <a href="javascript:void(0)" class="customBtn" data-bs-toggle="offcanvas" data-bs-target="#requestSample" aria-controls="offcanvasRight">Request Samples</a> 
                            <p class="pt-2"> <a class="link" href="products.php"><span class="icon-arrowleft icomoon"></span> Back to Products </a></p>
                         </article>
                     </div>
                 </div>
             </div>
         </div>
         <!--/ sub page header -->

         <!-- sub page body -->
         <div class="subpageBody pt-0">
            <div class="container">                
            <header id="header" class="header" data-scrollto-offset="0">
            <!-- product detail nav -->
            <nav class="navbar navbar-productdetail mt-md-2">
                <div class="d-flex d-md-none justify-content-between w-100 mobileNav px-2 px-md-0">
                    <h5>Flip Off Seals</h5>
                    <a href="javascript:void(0)" id="DetailDropdown"><i class="bi bi-list"></i></a>
                </div>
                <ul class="d-md-flex justify-content-md-between align-items-center" id="mobileItems">
                    <li><a class="nav-link scrollto" href="flipoffsealsnew.php#overview">Overview</a></li>
                    <li><a class="nav-link scrollto" href="flipoffsealsnew.php#productAttributes">Sizes</a></li>
                    <li><a class="nav-link scrollto" href="flipoffsealsnew.php#downloadableResources">Downloads</a></li>
                    <li><a class="nav-link scrollto" href="flipoffsealsnew.php#certifications">Certifications</a></li>                    
                    <li><a class="nav-link scrollto" href="flipoffsealsnew.php#relatedProducts">Related Products</a></li>
                </ul>
            </nav>
            <!--/ product detail nav -->
            </header>           
             </div>
             <!-- product detail description -->
             <section class="featured-services pt-3" id="overview">
              <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="h4 fsbold border-bottom pb-2 mb-3">Overview</h2>
                    </div>                                      
                    <div class="col-md-12 align-self-center">   
                        <h6 class="fsbold d-inline-block">Applications & Benefits </h6>
                        <ul class="list-items">
                            <li>Medical Applications (Surgical Equipment).</li>
                            <li>Pharmaceutical Machinery change Parts</li>
                            <li>Tubing sets</li>
                            <li>Longer durations for Autoclaved products & ETO products depending on the product and its storage conditions</li>
                            <li>Excellent strength and microbial barrier </li>
                        </ul>
                    </div>
                    <div class="col-md-12 align-self-center">   
                        <h6 class="fsbold d-inline-block">Features</h6>
                        <ul class="list-items">
                            <li>Excellent barrier properties against germs and moisture .</li>
                            <li>Extremely high penetration resistance </li>
                            <li>Easy and fiber-free peel </li>
                            <li>Tear and puncture-resistant</li>
                            <li>Hydrophobic and lint-free  </li>
                            <li>Excellent impact resistance with exceptional microbial barrier </li>
                        </ul>
                    </div>
                    <div class="col-md-12">
                        <h4 class="pb-2 h4 fbold">Sterile Pouches Gallery</h4>
                        <!-- gallery -->
                        <section class="gallery-block grid-gallery mt-0 pt-0 aos-item pb-0" data-aos="fade-up">
                            <div class="row g-2">
                                <div class="col-md-6 col-lg-3 col-6 item ">
                                    <a class="lightbox" href="img/products/sterilepouches/sterile01.jpg">
                                        <img class="img-fluid image scale-on-hover" src="img/products/sterilepouches/sterile01.jpg">
                                    </a>
                                </div>
                                <div class="col-md-6 col-lg-3 col-6 item">
                                    <a class="lightbox" href="img/products/sterilepouches/sterile02.jpg">
                                        <img class="img-fluid image scale-on-hover" src="img/products/sterilepouches/sterile02.jpg">
                                    </a>
                                </div>
                                <div class="col-md-6 col-lg-3 col-6 item">
                                    <a class="lightbox" href="img/products/sterilepouches/sterile03.jpg">
                                        <img class="img-fluid image scale-on-hover" src="img/products/sterilepouches/sterile03.jpg">
                                    </a>
                                </div>
                                <div class="col-md-6 col-lg-3 col-6 item">
                                    <a class="lightbox" href="img/products/sterilepouches/sterile04.jpg">
                                        <img class="img-fluid image scale-on-hover" src="img/products/sterilepouches/sterile04.jpg">
                                    </a>
                                </div>
                            </div>
                        </section>
                        <!--/ gallery -->
                    </div>
                </div>
                </div>
             </section>
              <section class="featured-services" id="productAttributes">
              <div class="container">
                  <div class="row justify-content-center">
                   <div class="col-md-12">
                    <h3 class="h4 fsbold border-bottom pb-2 mb-3">Available Sizes</h2>
                   </div>
                    <div class="col-md-12 text-center">                      
                        <div class="table-responsive pb-4">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">S.No:</th>
                                        <th scope="col" class="text-start">Size</th>                                   
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td class="text-start">30 x 45 CM Tyvek CPPET roll pouch</td>                                       
                                    </tr>
                                    <tr>
                                        <th scope="row">2</th>
                                        <td class="text-start">20 x 30 cm Angle pouch</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">3</th>
                                        <td class="text-start">12 x 37 Cm cylindrical pouch</td>                                      
                                    </tr>
                                    <tr>
                                        <th scope="row">4</th>
                                        <td class="text-start">10 x 50 Cm cylindrical pouch</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">5</th>
                                        <td class="text-start">30 x 45 cm roll pouch </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">6</th>
                                        <td class="text-start">7.5 x 37 cm cylindrical pouch</td>                                      
                                    </tr>
                                    <tr>
                                        <th scope="row">7</th>
                                        <td class="text-start">12 cm roll pouch</td>                                      
                                    </tr>                                   
                                </tbody>
                            </table>
                        </div>
                    </div>
                  </div>
                </div>
              
              </section>
             
              <section class="featured-services" id="downloadableResources">
              <div class="container">
                  <div class="row justify-content-center">
                        <div class="col-md-5">
                            <h2 class="text-center pb-4">Downloadable Resources</h2>
                            <div class="d-none">                           
                            <a href="img/dummy.pdf" download class="downloadItem"><span class="icon-download"></span> Downloadable Component Name will be here</a>
                            <a href="img/dummy.pdf" download  class="downloadItem"><span class="icon-download"></span> Downloadable Component Name will be here</a>
                            </div>
                            <p class="text-center h5 fwhite">Coming soon</p>
                      </div>
                  </div>
              </div>r
              </section>
              <section class="featured-services" id="certifications">
                <div class="container">
                  <div class="row justify-content-center">
                    <div class="col-md-12">
                        <h3 class="h4 fsbold border-bottom pb-2 mb-3">Certifications</h2>
                    </div>
                  </div>               

                   <!-- certifications -->
                   <div class="certificateImages py-3 d-flex justify-content-between">
                        <?php include 'includes/certificates.php' ?>
                    </div>
                    <!--/ certifications -->
                </div>
               
              </section>
              <section class="featured-services realtedProducts pt-0" id="relatedProducts">
              <div class="container">                 
                  <h3 class="h4 fsbold pb-2 mb-3">Related Products</h2>

                <div class="swiper-container w-100 productsSliderProductDetail">
                <div class="swiper-wrapper">
                    <!-- swiper slider -->
                    <?php
                    for($i=0; $i<count($productSliderItem);$i++){ ?>
                    <div class="swiper-slide">
                         <div class="card productCard mb-3">
                            <a href="<?php echo $productItem[$i][0]?>">
                                <img src="img/products/<?php echo $productItem[$i][1]?>" alt="" class="img-fluid">
                            </a>
                            <div class="card-body">
                                <h5 class="card-title"><?php echo $productItem[$i][2]?></h5>
                                <p class="card-text"><?php echo $productItem[$i][3]?></p>
                                <a href="<?php echo $productItem[$i][0]?>" class="card-link">Read More &rarr; </a>
                            </div>
                        </div>
                    </div>   
                   <?php } ?>
                    <!--/ swiper slider -->                 
                </div>
                <!-- Add Arrows -->
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
            </div>
              </div>
              </section>
             <!-- product detail description -->
         </div>
         <!--/ sub page body -->
     </main>
    <!-- footer -->
     <?php include 'includes/footer.php'?>
    <!--/ footer -->
     <?php include 'includes/scripts.php'?>

     <script>
        $(document).ready(function(){
            $("#DetailDropdown").click(function(){
                $("#mobileItems").toggle();
            });
        });
    </script>

     <script>
        window.onscroll = function() {myFunction()};
        var navbar = document.getElementById("header");
        var sticky = navbar.offsetTop;
        function myFunction() {
        if (window.pageYOffset >= sticky) {
            navbar.classList.add("stickynav")
        } else {
            navbar.classList.remove("stickynav");
        }
        }
        </script>

    <!-- Modal video -->
    <div class="modal fade video-modal productVideo"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog  modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">  
                    <h5 class="modal-title">Flip Off Seals</h5>             
                    <button id="close" type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span class="icon-close icomoon"></span>
                    </button>
                </div>
                <div class="modal-body">                

                    <iframe id="ytplayer" class="modalvideo" width="100%" src="https://www.youtube.com/embed/v_I3Dpay90w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
    <!--/ modal video -->

    <script>   
        $('.video-modal').on('hidden.bs.modal', function (e) {
            $('.video-modal iframe').attr('src', '');
        });

        $('.video-modal').on('show.bs.modal', function (e) {
            $('.video-modal iframe').attr('src', 'https://www.youtube.com/embed/v_I3Dpay90w?rel=0&amp;autoplay=1');
        });
    </script>


<!-- request quotation -->
<div class="offcanvas offcanvas-end" tabindex="-1" id="requestQuotation" aria-labelledby="offcanvasRightLabel">
  <div class="offcanvas-header">
    <h5 id="offcanvasRightLabel">Request for Quotation</h5>
    <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
  </div>
  <div class="offcanvas-body">
    <div class="careerRightSection">
        <?php include 'includes/requestquotation.php'?>
        <p class="pt-3">
            <span class="fsbold">Note:</span> After submit your request, Our Team will connect you soon.
        </p>
        </div>
    </div>
</div>
<!--/ request quotation -->

<!-- request sample -->
<div class="offcanvas offcanvas-end" tabindex="-1" id="requestSample" aria-labelledby="offcanvasRightLabel">
  <div class="offcanvas-header">
    <h5 id="offcanvasRightLabel">Request Samples</h5>
    <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
  </div>
  <div class="offcanvas-body">
    <div class="careerRightSection">
         <?php include 'includes/requestsampleform.php'?>
        <p class="pt-3">
            <span class="fsbold">Note:</span> After submit your request, Our Team will connect you soon.
        </p>
        </div>
    </div>
</div>
<!--/ request sample -->

</body>

</html>