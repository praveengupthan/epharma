<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>About East Pharma</title>    
    <?php include 'includes/styles.php'?>
    <?php include 'includes/data.php'?>
</head>

<body>
    <?php include 'includes/header.php'?>
     <main class="subPage">
         <!-- sub page header -->
         <div class="subPageHeader">
             <div class="container">
                 <div class="row">
                     <div class="col-md-6 leftsubpageHeader align-self-center">
                         <h1>About us</h1>
                     </div>
                     <div class="col-md-6 align-self-center">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo $homeLink?>"><?php echo $SPageHome ?></a></li>                              
                                <li class="breadcrumb-item active" aria-current="page">About</li>
                            </ol>
                        </nav>
                     </div>
                 </div>
             </div>
         </div>
         <!--/ sub page header -->

         <!-- sub page body -->
         <div class="subpageBody">
             <div class="container">
                 <!-- row -->
                 <div class="row">
                     <div class="col-md-6 aos-item" data-aos="fade-up">
                         <figure>
                              <img src="img/aboutImg.jpg" alt="" class="img-fluid w-100 ">
                         </figure>
                     </div>
                     <div class="col-md-6 align-self-center aos-item" data-aos="fade-down">
                         <h6>Introduction</h6>
                         <h2 class="py-3">Welcome to East Pharma</h2>
                         <ul class="list-items">
                            <li>Founded with the vision to be firmly establish as one of the world’s best in class products with innovative technology.</li>
                            <li>Located in South of India, Hyderabad in the industrial Hub. 30 mins drive from heart of the city</li>
                            <li>We thrive to provide highest quality packaging & rubber products at affordable prices with timely deliveries.</li>
                            <li>Most advanced facility conforming to the cGMP and international standards. </li>
                            <li>Every product manufactured have exclusive DMF. </li>
                            <li>Equipped with in-house tool room for customer custom requirements.</li>
                            <li>Dedicated experienced team.</li>                           
                         </ul>
                     </div>
                 </div>
                 <!--/ row -->
                </div>
                <div class="corevaluesSection">
                    <div class="container">
                         <!-- row -->
                        <div class="row pt-2 pt-md-5 justify-content-center">                    
                            <div class="col-md-10 text-center">
                                <article class="py-3 aos-item" data-aos="fade-up">
                                    <h5 class="h4">We at east pharma technologies, are committed to enhance customer satisfaction by manufacturing and supply of high quality packaging products through implementation and continual improvement of quality management system.</h5>                           
                                </article>
                            </div>
                            <div class="col-md-6 text-center aos-item" data-aos="fade-down">
                                <img src="img/corevaluesvector.svg" alt="" class="img-fluid">
                            </div>
                        </div>
                        <!--/ row -->
                    </div>
                 </div>

                 <div class="container">
                    <!-- visioin and mission -->
                    <div class="visionMission row">
                        <div class="col-lg-4 aos-item" data-aos="fade-up">
                            <div class="vmCol text-center greencol">
                                <span class="icon-mountain icomoon"></span>
                                <article>
                                    <h5 class="h4 fsbold">Vision</h5>
                                    <p>Maintain our position as an adaptable, flexible and cost effective supplier to our valued customers, continually looking to future needs and demands of the market to redefine and manage our service, Respect and anticipate the requirements of regulations and our customers</p>
                                </article>
                            </div>
                        </div>
                        <div class="col-lg-4 aos-item" data-aos="fade-down">
                            <div class="vmCol text-center bluecol">
                                <span class="icon-mission icomoon"></span>
                                <article>
                                    <h5 class="h4 fsbold">Mission</h5>
                                    <p>To be one of the world’s best choice destination for the most challenging primary & secondary packaging & other products.</p>
                                </article>
                            </div>
                        </div>
                        <div class="col-lg-4 aos-item" data-aos="fade-up">
                            <div class="vmCol greencol2">
                                <span class="icon-value icomoon"></span>
                                <article>
                                    <h5 class="h4 fsbold">Values</h5>
                                     <ul>
                                        <li>Economical</li>                                        
                                        <li>Quality</li>
                                        <li>Responsive</li>
                                        <li>Timely Delivery</li>
                                        <li>Customer Satisfaction</li>
                                        <li>Continual Improvement</li>
                                     </ul>
                                </article>
                            </div>
                        </div>
                    </div>
                    <!--/ visiion and missioin -->
                 </div>
            
         </div>
         <!--/ sub page body -->
     </main>
    <!-- footer -->
     <?php include 'includes/footer.php'?>
    <!--/ footer -->
     <?php include 'includes/scripts.php'?>
</body>

</html>