<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contact </title>    
    <?php include 'includes/styles.php'?>
    <?php include 'includes/data.php'?>
</head>

<body>
    <?php include 'includes/header.php'?>
     <main class="subPage">
         <!-- sub page header -->
         <div class="subPageHeader">
             <div class="container">
                 <div class="row">
                     <div class="col-md-6 leftsubpageHeader align-self-center">
                         <h1>Contact</h1>
                     </div>
                     <div class="col-md-6 align-self-center">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo $homeLink?>"><?php echo $SPageHome?></a></li>
                                <li class="breadcrumb-item active" aria-current="page">Contact</li>
                            </ol>
                        </nav>
                     </div>
                 </div>
             </div>
         </div>
         <!--/ sub page header -->

         <!-- sub page body -->
         <div class="subpageBody">
             <div class="container">
             <div class="addressCol p-3 border mb-3">
                <div class="row">                
                    <div class="col-md-6 align-self-center">
                        <div class="address table-responsive">
                            <h5 class="border-bottom pb-2 mb-2 fsbold">Corporate office address</h5>
                            <table class="table table-borderless">
                                <tr>
                                    <td class="leftlabel">Address</td>
                                    <td>
                                        <h5 class="h6"><b>East pharma technologies</b></h5>
                                        <p class="text-left pb-0">Level 21, Galaxy, Opp. IKEA, Hyderabad Knowledge city,
                                        Ranga Reddy (Dt), Hyderabad,Telangana, India - 500032</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="leftlabel">Contact</td>
                                    <td>
                                        <p class="pb-0">(+91) 99122 84900, (+91) 9440919427 </p>                                    
                                    </td>
                                </tr>
                                <tr>
                                    <td class="leftlabel">Email</td>
                                    <td>
                                        <p class="pb-0">info@eastpharmatechnologies.com</p>                                    
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-6">
                         <!-- map-->
                         <div class="map mt-2">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7609.329369734488!2d78.168573!3d17.523513!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x406cf8ae876cccaa!2sEAST%20PHARMA%20TECHNOLOGIES!5e0!3m2!1sen!2sin!4v1633692224392!5m2!1sen!2sin" width="100%" height="300" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                        </div>
                        <!--/map-->
                    </div>
                 </div>
            </div>
            <div class="addressCol p-3 border mb-3">
                <div class="row">                
                    <div class="col-md-6 align-self-center order-lg-last">
                        <div class="address table-responsive">
                            <h5 class="border-bottom pb-2 mb-2 fsbold">Manufacturing Unit</h5>
                            <table class="table table-borderless">
                                <tr>
                                    <td class="leftlabel">Address</td>
                                    <td>
                                        <h5 class="h6"><b>East pharma technologies</b></h5>
                                        <p class="text-left pb-0">SDF block-1, Ground Floor, EPIP, TSIIC, Patancheru (Mdl), Sangareddy (D), Hyderabad, Telangana - 502307, India.</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="leftlabel">Contact</td>
                                    <td>
                                        <p class="pb-0">(+91) 9000004304, (+91) 9440919427 </p>          
                                    </td>
                                </tr>
                                <tr>
                                    <td class="leftlabel">Email</td>
                                    <td>
                                        <p class="pb-0">info@eastpharmatechnologies.com</p>                                    
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-6">
                         <!-- map-->
                         <div class="map mt-2">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7609.329369734488!2d78.168573!3d17.523513!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x406cf8ae876cccaa!2sEAST%20PHARMA%20TECHNOLOGIES!5e0!3m2!1sen!2sin!4v1633692224392!5m2!1sen!2sin" width="100%" height="300" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                        </div>
                        <!--/map-->
                    </div>
                 </div>
            </div>
          

               <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">                    
              
            <?php     

if(isset($_POST['submitContact'])){
$to = "info@eastpharmatechnologies.com"; 
$subject = "Mail From ".$_POST['name'];
$message = "
<html>
<head>
<title>HTML email</title>
</head>
<body>
<p>".$_POST['name']." has sent mail!</p>
<table>
<tr>
<th align='left'>Name</th>
<td>".$_POST['name']."</td>
</tr>
<tr>
<th align='left'>Contact Number</th>
<td>".$_POST['phone']."</td>
</tr>
<tr>
<th align='left'>Email</th>
<td>".$_POST['email']."</td>
</tr>
<tr>
<th align='left'>How did you first hear of us</th>
<td>".$_POST['howDid']."</td>
</tr>
<tr>
<th align='left'>Subject</th>
<td>".$_POST['sub']."</td>
</tr>
<tr>
<th align='left'>Message</th>
<td>".$_POST['msg']."</td>
</tr>
</table>
</body>
</html>
";

// Always set content-type when sending HTML email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
$headers .= 'From:' .$_POST['name']. "\r\n";
//$headers .= 'Cc: myboss@example.com' . "\r\n";

mail($to,$subject,$message,$headers);   

//success mesage
?>
<div class="alert alert-success alert-dismissible fade show" role="alert">
  Mail Sent Successfully. Thank you <?= $_POST['name'] ?>, we will contact you shortly.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<?php

// echo "<p style='color:green'>Mail Sent. Thank you " . $_POST['name'] . ", we will contact you shortly.</p>";
}
?>
                         

                    <!-- form -->
                    <form id="contact_form" class="form customForm mt-md-5" action="" method="post">
                    <h3 class="h4 fsbold pb-3">Let’s build something great together</h3>              
                        <!-- row -->
                        <div class="row">
                            <!-- col -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Name</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="name" placeholder="Write Your Name">
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-md-6">
                                <div class="form-group">
                                    <label>Phone Number (Optional)</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="phone" placeholder="Enter Valid Phne" >
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-md-6">
                                <div class="form-group">
                                    <label>Email Address</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="email" placeholder="Write Email">
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->                           
                             
                            <!-- col -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>How did you first hear of us</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="howDid" placeholder="Write How did you know about us" >
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->
                        <!-- col -->
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Subject</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="sub" placeholder="Subject" >
                                </div>
                            </div>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Message</label>
                                <div class="input-group">
                                    <textarea class="form-control" name="msg" style="height:100px;" placeholder="Write Message"></textarea>
                                </div>
                            </div>
                        </div>
                        <!--/ col -->
                          <!-- col -->
                          <div class="col-md-12">                           
                               <button class="btn btn-info w-100 text-uppercase" name="submitContact">Submit</button>                            
                          </div>
                        <!--/ col -->
                        </div>
                        <!--/ row -->
                    </form>
                    <!--/ form -->
                </div>
                <!--/ col -->
            </div>
            <!--/row --> 
             </div>
              
                 
         </div>
         <!--/ sub page body -->
     </main>
    <!-- footer -->
     <?php include 'includes/footer.php'?>
    <!--/ footer -->
     <?php include 'includes/scripts.php'?>
</body>
</html>