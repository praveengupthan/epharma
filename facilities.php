<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Facilities </title>    
    <?php include 'includes/styles.php'?>
    <?php include 'includes/data.php'?>
</head>

<body>
    <?php include 'includes/header.php'?>
     <main class="subPage">
         <!-- sub page header -->
         <div class="subPageHeader">
             <div class="container">
                 <div class="row">
                     <div class="col-md-6 leftsubpageHeader align-self-center">
                         <h1>Facilities</h1>
                     </div>
                     <div class="col-md-6 align-self-center">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo $homeLink?>"><?php echo $SPageHome?></a></li>
                                <li class="breadcrumb-item active" aria-current="page">Facilities</li>
                            </ol>
                        </nav>
                     </div>
                 </div>
             </div>
         </div>
         <!--/ sub page header -->

         <!-- sub page body -->
         <div class="subpageBody">
             <div class="container">
                 <!-- row -->
                 <div class="row">
                    <div class="col-md-6 aos-item" data-aos="fade-up">
                        <figure class="videoImg position-relative">
                            <a href="javascript:void(0)" class="customBtn videoButton" data-bs-toggle="modal" data-bs-target=".productVideo"><span class="icon-play-button"></span> </a>
                            <img src="img/facilities01.jpg" alt="" class="img-fluid w-100">
                        </figure>
                    </div>
                    <div class="col-md-6 align-self-center aos-item" data-aos="fade-down">
                        <h2>Facilities of Infrastructure and Production</h2>
                        <ul class="list-items">
                            <li>We are equipped with state of art facility with latest technology in the Injection Moulding, Punching, Assembly and Camera Inspection units.</li>                           
                            <li>Our Core team is qualified with vast 20 years ofexperience in Pharma industry.</li>                        
                        </ul>                       
                    </div>                   
                 </div>
                 <!-- row -->                

                    <!-- row -->
                    <div class="row">
                    <div class="col-md-6 order-lg-last aos-item" data-aos="fade-up">
                        <figure class="videoImg">
                            <img src="img/facilities03.jpg" alt="" class="img-fluid w-100">
                        </figure>
                    </div>
                    <div class="col-md-6 align-self-center aos-item" data-aos="fade-down">
                        
                         <h2 class="pt-3">Factory & Infrastructure</h2>
                         <ul class="list-items">                           
                            <li>Our Plastic components are being made using hot runner mould system gives product with highest accuracy & consistent quality with respect to Shape, Colour & Weight.</li>
                            <li>Our products come with Flip free technology which ensures burr free, smooth, clean and safe surface where the medicine is taken out.</li> 
                            <li>Aluminium components are washed and dried to eliminate any traces of oil being used in punching operation.</li>                       
                            <li>Our Assembling equipments deliver products with clean and uniform riveting.</li>
                            <li>In house Tool Room to support punching and other operations.</li>
                            <li>All components are passed through a six camera inspection system to ensure quality of product in all respect.</li>
                            <li>Products are manufactured in clean and ambient environment with centralized AHU.</li>
                        </ul>     
                        
                    </div>                   
                 </div>
                 <!-- row -->


                  <!-- gallery -->
                  <div class="col-md-12 pt-3">
                        <h3 class="pb-2">Facilities of Art Gallery</h3>
                         <section class="gallery-block grid-gallery mt-0 pt-0 aos-item" data-aos="fade-up">
                            <div class="row">
                                <div class="col-md-6 col-lg-4 col-6 item ">
                                    <a class="lightbox" href="img/gallery/galimg01.jpg">
                                        <img class="img-fluid image scale-on-hover" src="img/gallery/galimg01.jpg">
                                    </a>
                                </div>
                                <div class="col-md-6 col-lg-4 col-6 item">
                                    <a class="lightbox" href="img/gallery/galimg02.jpg">
                                        <img class="img-fluid image scale-on-hover" src="img/gallery/galimg02.jpg">
                                    </a>
                                </div>
                                <div class="col-md-6 col-lg-4 col-6 item">
                                    <a class="lightbox" href="img/gallery/galimg03.jpg">
                                        <img class="img-fluid image scale-on-hover" src="img/gallery/galimg03.jpg">
                                    </a>
                                </div>                               
                                <div class="col-md-6 col-lg-4 col-6 item">
                                    <a class="lightbox" href="img/gallery/galimg04.jpg">
                                        <img class="img-fluid image scale-on-hover" src="img/gallery/galimg04.jpg">
                                    </a>
                                </div>
                                <div class="col-md-6 col-lg-4 col-6 item">
                                    <a class="lightbox" href="img/gallery/galimg05.jpg">
                                        <img class="img-fluid image scale-on-hover" src="img/gallery/galimg05.jpg">
                                    </a>
                                </div>
                                <div class="col-md-6 col-lg-4 col-6 item">
                                    <a class="lightbox" href="img/gallery/galimg06.jpg">
                                        <img class="img-fluid image scale-on-hover" src="img/gallery/galimg06.jpg">
                                    </a>
                                </div>
                                <div class="col-md-6 col-lg-4 col-6 item">
                                    <a class="lightbox" href="img/gallery/galimg07.jpg">
                                        <img class="img-fluid image scale-on-hover" src="img/gallery/galimg07.jpg">
                                    </a>
                                </div>
                                <div class="col-md-6 col-lg-4 col-6 item">
                                    <a class="lightbox" href="img/gallery/galimg08.jpg">
                                        <img class="img-fluid image scale-on-hover" src="img/gallery/galimg08.jpg">
                                    </a>
                                </div>    
                                 <div class="col-md-6 col-lg-4 col-6 item">
                                    <a class="lightbox" href="img/gallery/galimg09.jpg">
                                        <img class="img-fluid image scale-on-hover" src="img/gallery/galimg09.jpg">
                                    </a>
                                </div>                           
                            </div>                           
                        </section>
                    </div>
                    <!--/ gallery -->
             </div>
         </div>
         <!--/ sub page body -->
     </main>
    <!-- footer -->
     <?php include 'includes/footer.php'?>
    <!--/ footer -->
     <?php include 'includes/scripts.php'?>

      <!-- Modal video -->
    <div class="modal fade video-modal productVideo"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog  modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">  
                    <h5 class="modal-title">Flip Off Seals</h5>             
                    <button id="close" type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span class="icon-close icomoon"></span>
                    </button>
                </div>
                <div class="modal-body"> 
                    <iframe id="ytplayer" class="modalvideo" width="100%" src="https://www.youtube.com/embed/v_I3Dpay90w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
    <!--/ modal video -->

     <script>   
        $('.video-modal').on('hidden.bs.modal', function (e) {
            $('.video-modal iframe').attr('src', '');
        });

        $('.video-modal').on('show.bs.modal', function (e) {
            $('.video-modal iframe').attr('src', 'https://www.youtube.com/embed/v_I3Dpay90w;autoplay=1');
        });
    </script>


</body>
</html>